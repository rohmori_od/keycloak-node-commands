import { readFile } from "fs";
import { join } from "path";
import * as yargs from "yargs";
import { KeycloakConnection, KeycloakConnectionConfig } from "./src/keycloak";

const test = yargs(process.argv.slice(2)).command(
    "test",
    "Test Connection"
).argv;

if (test) {
    readFile(join(__dirname, "config.json"), (err, data) => {
        if (err) {
            return console.error("Could not read config. Did npm prepare run?");
        }
        try {
            const config = JSON.parse(
                data.toString()
            ) as KeycloakConnectionConfig;
            console.log("Using config: ", config);

            const keycloak = new KeycloakConnection(config);
            keycloak.connect();
        } catch (e) {
            console.error(e);
        }
    });
}
