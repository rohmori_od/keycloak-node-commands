# keycloak-node-commands readme

https://www.keycloak.org/getting-started/getting-started-docker

1. Install Docker Desktop. Restart computer
2. Open terminal and run this command `docker run -p 8080:8080 -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin quay.io/keycloak/keycloak:17.0.0 start-dev` (takes a while)
3. Install node.js if not already installed
4. Run `npm i`
5. Run `npm run test` (this is not working yet; the connection works but permissions are not set up by default)
