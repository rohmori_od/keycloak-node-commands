export interface CommonConnectionConfig {
    host: string;
}

export interface ConnectorOptions {
    logLevel: LogLevel;
}

export enum LogLevel {
    ALL,
    WARN,
    ERROR,
}
