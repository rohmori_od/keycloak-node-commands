import axios from "axios";
import { AxiosRequestConfig } from "axios";
import { stringify } from "querystring";
import { CommonConnectionConfig } from "./types";

export interface KeycloakConnectionConfig extends CommonConnectionConfig {
    realm: string;
    clientId: string;
    clientSecret: string;
    accountName: string;
    password: string;
}

export class KeycloakConnection {
    constructor(private config: KeycloakConnectionConfig) {
        axios.defaults.baseURL = config.host;
    }

    connect() {
        this.authenticate();
    }

    private authenticate() {
        const config = this.config;

        const loginArgs = {
            client_id: config.clientId,
            client_secret: config.clientSecret,
            grant_type: "client_credentials",
            scope: "openid",
            username: config.accountName,
            password: config.password,
        };
        const url =
            "/realms/" + config.realm + "/protocol/openid-connect/token";
        const requestOptions: AxiosRequestConfig = {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            },
        };

        console.log(url);
        axios
            .post(url, stringify(loginArgs), requestOptions)
            .then((res) => console.log(res));
    }
}
