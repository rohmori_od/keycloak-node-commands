import { writeFile } from "fs";
import { join } from "path";

// This file could be made to prompt the user for all of these variables

const defaultConfig = {
    host: "http://localhost:8080",
    realm: "master",
    clientId: "account",
    clientSecret: "",
    accountName: "admin",
    password: "admin",
};

writeFile(
    join(__dirname, "config.json"),
    JSON.stringify(defaultConfig),
    (err: unknown) => {
        if (err) {
            console.error(err);
            return;
        }
    }
);
